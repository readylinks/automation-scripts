# ReadyLinks.io Automation Scripts

## Description

The ReadyLinks API is organized around REST. Our API has predictable resource-oriented URLs, returns JSON-encoded responses, and uses standard HTTP response codes, authentication, and verbs. 

### Live interactive API is available at [https://api.readylinks.io/docs](https://api.readylinks.io/docs).

What can you do with the ReadyLinks API?

Add new networks, devices, VLANs and more
Configure thousands of networks in minutes
Gather live data from thousands of end points in minutes
Build your own dashboard integrations and custom reports with live data

You can quickly get up and running by using your Organization ID as your username and API Key as your password for each route. Your unique organization ID and API Key can each be found in your ReadyView.io account.