#
# Applies Syslogs for devices in a network
# Authentication uses org_id and api_key, found under "Account" at https://ui.readylinks.io
# Input your network ID on line 70. Network IDs may be found in ReadyView or via API
# To execute: python3 readylinks_async_apply_syslogs.py

import asyncio
import httpx

from pydantic import (
    BaseModel, 
    Field
)
from typing import (
    List,
    Optional
)

org_id = "your-org-key"
api_key = "your-api-key"


class DeviceInfo(BaseModel):
    status: str = Field(..., description="Current status of the device")
    mac_address: str = Field(..., description="MAC address of the device")
    part_number: str = Field(..., description="Part number of the device")
    description: Optional[str] = Field(None, description="Description of the device")
    location: Optional[str] = Field(None, description="Location of the device")
    contact: Optional[str] = Field(None, description="Contact information for the device")
    firmware_version: str = Field(..., description="Firmware version installed on the device")
    alarmed: Optional[bool] = Field(None, description="Flag indicating if the device is in an alarmed state")
    rlid: Optional[str] = Field(None, description="ReadyLinks ID for the device")
    latitude: Optional[str] = Field(None, description="Latitude coordinate of the device's location")
    longitude: Optional[str] = Field(None, description="Longitude coordinate of the device's location")

class SysLogInfo(BaseModel):
    enabled: bool = Field(..., description="Enable remote syslog")
    server_ip_address: str = Field(..., description="Syslog server IP address")
    destination_port: int = Field(..., description="Syslog service port")
    log_level: str = Field(..., description="Log level to send to the syslog server")

class DeviceSettings(BaseModel):
    syslog: SysLogInfo = Field(..., description="System log settings")

class ReadyViewAPI:
    def __init__(self, api_key: str, organization: str, uri: str = "https://api.readylinks.io"):
      self.auth = organization, api_key
      self.uri = uri

    async def get(self, url: str) -> httpx.Response:
        async with httpx.AsyncClient(timeout=10) as client:
              try:
                  res = await client.get(
                      url=url,
                      auth=self.auth
                  )
                  res.raise_for_status()
                  return res
              except httpx.HTTPStatusError as e:
                  print(f"httpx.HTTPError - {type(e).__name__,} - {e} - {e.response.text}")
                  raise e
              except httpx.HTTPError as e:
                  print(f"httpx.HTTPError - {type(e).__name__,} - {e}")
                  raise e
              except Exception as e:
                  print(f"Exception occurred - {type(e).__name__,} - {e}")
                  raise e
    async def post(self, url: str, body: dict) -> httpx.Response:
        async with httpx.AsyncClient(timeout=25) as client:
              try:
                  res = await client.post(
                      url=url,
                      auth=self.auth,
                      json=body
                  )
                  res.raise_for_status()
                  return res
              except httpx.HTTPStatusError as e:
                  print(f"httpx.HTTPError - {type(e).__name__,} - {e} - {e.response.text}")
                  raise e
              except httpx.HTTPError as e:
                  print(f"httpx.HTTPError - {type(e).__name__,} - {e}")
                  raise e
              except Exception as e:
                  print(f"Exception occurred - {type(e).__name__,} - {e}")
                  raise e
              
    async def get_devices_in_network(self, network: str) -> List[DeviceInfo]:
        res = await self.get(
            url = f"{self.uri}/v1/networks/{network}/devices"
        )
        return [DeviceInfo(**device) for device in res.json()]

    async def apply_device_settings(self, mac_address: str, body: dict):
        res = await self.post(
            url = f"{self.uri}/v1/devices/{mac_address}/switch",
            body = body
        )
        response_data = res.json()
    
        # Check if any field in the response data has a value 'Failed'
        if any(value == 'Failed' for value in response_data.values()):
            raise ValueError(f"One or more settings failed to apply - {response_data}")

        return res


async def main():

    # Asynchronously apply syslogs of all devices in a network
    # ------------------------------------------------------------

    # Input your Network ID Here,  all network IDs may be found in ReadyView or via API
    net_id = "network-id"
    # 
    
    global api_key, org_id
    api = ReadyViewAPI(
        api_key = api_key,
        organization = org_id
    )

    devices = await api.get_devices_in_network(network=net_id)

    for device in devices:
        if device.status != "Online":
            print(f"Device {device.mac_address} is {device.status}. Please bring this device 'Online' to apply settings.")
            continue
        if float(device.firmware_version) < 7.655:
            print(f"Device {device.mac_address} is on firmware {device.firmware_version}. Please upgrade this device to firmware '7.655' or greater.")
            continue

        res = await api.apply_device_settings(
            mac_address = device.mac_address,
            body = {
                'syslog': {
                    'enabled': True,
                    'server_ip_address': "10.10.200.110",
                    'destination_port': 30003,
                    'log_level': "all"
                }
            }
        )

        settings = DeviceSettings(**res.json())

        print(f"Applied setting - {device.mac_address} - {settings}")

asyncio.run(main())