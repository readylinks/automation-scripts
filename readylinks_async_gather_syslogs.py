#
# Gathers Syslogs for devices in a network
# Authentication uses org_id and api_key, found under "Account" at https://ui.readylinks.io
# Input your network ID on line 70. Network IDs may be found in ReadyView or via API
# To execute: python3 readylinks_async_gather_syslogs.py

import asyncio
import httpx
import json

org_id = "your-org-key"
api_key = "your-api-key"
auth_creds = (org_id, api_key)


async def get_switch(mac):
    url = "https://api.readylinks.io/v1/devices/" + mac + "/switch"
    for _ in range(3):  # Retry up to 3 times
        try:
            timeout = httpx.Timeout(10.0, connect=5.0)
            async with httpx.AsyncClient(timeout=timeout) as client:
                response = await client.get(
                    url, auth=auth_creds, headers={"accept": "application/json"}
                )
                if response.status_code != 200:
                    continue  # If the status code is not 200, retry the request
                switch = response.json()
                return switch
        except httpx.HTTPError:
            continue  # If an HTTPError is raised, retry the request
    return None  # If the request fails 3 times, return None


async def get_networks():
    url = "https://api.readylinks.io/v1/networks"
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                url, auth=auth_creds, headers={"accept": "application/json"}
            )
            networks = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)

    return networks


async def get_devices_in_network(network_id):
    url = "https://api.readylinks.io/v1/networks/" + network_id + "/devices"
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                url, auth=auth_creds, headers={"accept": "application/json"}
            )
            devices = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)

    return devices


async def main():

    # Asynchronously gather syslogs of all devices in a network
    # ------------------------------------------------------------

    # Input your Network ID Here,  all network IDs may be found in ReadyView or via API
    net_id = "1HH6KHun1GBakWIlE9Lq"
    # 

    net_devices = await get_devices_in_network(net_id)
    net_devices = json.loads(net_devices)

    # Async get syslogs of all devices in a network
    tasks = []
    try:
        for device in net_devices:
            mac = device["mac_address"]
            # The API will handle instances where a device is already on the template, or if the template does not match the device type
            tasks.append(get_switch(mac))

        # Runs all tasks concurrently and return a list of results upon completion 
        results = await asyncio.gather(*tasks)
        # Filter out None results
        results = [result for result in results if result is not None]  
        if results: 
            for result in results:
                mac_address = result['mac_address']
                model = result['model']
                name = result['name']
                location = result['location']
                description = result['description']
                cpu_utilization = result['cpu_utilization']
                memory_usage = result['memory_usage']
                logs = result['logs']
                print(f"""
                System Logs for: {mac_address}
                Name: {name}
                Description: {description}
                Model: {model}
                Location: {location}
                CPU Utilization: {cpu_utilization}
                Memory Usage: {memory_usage}
                Logs: {logs}
                """)
        else:
            print('No results found')
    except Exception as e:
        print(e)
        return 
    return 

asyncio.run(main())