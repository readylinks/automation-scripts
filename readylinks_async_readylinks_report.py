# Builds a ReadyLinks report for all devices within organization
# Authentication uses org_id and api_key, found under "Account" at https://ui.readylinks.io
# To execute: python3 readylinks_async_readylinks_report.py

import httpx
import pandas as pd
import asyncio
import datetime

from pydantic import (
    BaseModel,
    Field
)
from typing import (
    List,
    Optional,
    Any
)

org_id = "your-org-key"
api_key = "your-api-key"

class NetworkDocument(BaseModel):
    id: str = Field(example="dklnfojwenokdo", description="ID for the document")
    created: Any = Field(..., description="When the document was created")
    modified: Any = Field(..., description="When the document was last modified")

class NetworkInfo(BaseModel):
    name: str = Field(default="Las Vegas Airport", description="Name of the network location")

class SwitchInfo(BaseModel):
    uptime: Optional[float] = Field(examples=[262245], description="Uptime in seconds of the device.")
    hardware_version: Optional[str] = Field(example="3.0", description="Hardware version of the device.")
    cpu_utilization: Optional[str] = Field(examples=["25.54%"], description="Live CPU utilization of the device.")
    bootloader_version: Optional[float] = Field(examples=[1.000], description="Bootloader version of the device.")
    logs: Optional[List] = Field(description="System logs of the device.")
    serial_number: Optional[str] = Field(example="R3A0176910", description="The serial number of the device.")
    temperature: Optional[str] = Field(examples=["44.1"], description="Temperature in Celsius of the device.")
    memory_usage: Optional[str] = Field(examples=["50.1%"], description="Live memory usage of the device.")
    #
    # TODO expand attributes based on the documented api
    # https://api.readylinks.io/redoc#tag/Switches/operation/get_switch_configurations_v1_devices__mac_address__switch_get
    #---------------------------------------------------

class DeviceInfo(BaseModel):
    status: str = Field(..., description="Current status of the device")
    mac_address: str = Field(..., description="MAC address of the device")
    part_number: str = Field(..., description="Part number of the device")
    description: Optional[str] = Field(None, description="Description of the device")
    location: Optional[str] = Field(None, description="Location of the device")
    contact: Optional[str] = Field(None, description="Contact information for the device")
    firmware_version: str = Field(..., description="Firmware version installed on the device")
    alarmed: Optional[bool] = Field(None, description="Flag indicating if the device is in an alarmed state")
    id: Optional[str] = Field(None, description="ReadyLinks ID for the device")
    latitude: Optional[str] = Field(None, description="Latitude coordinate of the device's location")
    longitude: Optional[str] = Field(None, description="Longitude coordinate of the device's location")

class SysLogInfo(BaseModel):
    enabled: bool = Field(..., description="Enable remote syslog")
    server_ip_address: str = Field(..., description="Syslog server IP address")
    destination_port: int = Field(..., description="Syslog service port")
    log_level: str = Field(..., description="Log level to send to the syslog server")

class DeviceSettings(BaseModel):
    syslog: SysLogInfo = Field(..., description="System log settings")

class ReadyViewAPI:
    def __init__(self, api_key: str, organization: str, uri: str = "https://api.readylinks.io"):
      self.auth = organization, api_key
      self.uri = uri

    async def get(self, url: str) -> httpx.Response:
        async with httpx.AsyncClient(timeout=25) as client:
              try:
                  res = await client.get(
                      url=url,
                      auth=self.auth
                  )
                  res.raise_for_status()
                  return res
              except httpx.HTTPStatusError as e:
                  print(f"httpx.HTTPError - {type(e).__name__,} - {e} - {e.response.text}")
                  raise e
              except httpx.HTTPError as e:
                  print(f"httpx.HTTPError - {type(e).__name__,} - {e}")
                  raise e
              except Exception as e:
                  print(f"Exception occurred - {type(e).__name__,} - {e}")
                  raise e

    async def post(self, url: str, body: dict) -> httpx.Response:
        async with httpx.AsyncClient(timeout=25) as client:
              try:
                  res = await client.post(
                      url=url,
                      auth=self.auth,
                      json=body
                  )
                  res.raise_for_status()
                  return res
              except httpx.HTTPError as e:
                  print(f"httpx.HTTPError - {type(e).__name__,} - {e}")
                  raise e
              except Exception as e:
                  print(f"Exception occurred - {type(e).__name__,} - {e}")
                  raise e

    async def get_networks(self) -> List[NetworkDocument]:
        res = await self.get(
            url = f"{self.uri}/v2/networks"
        )
        return [NetworkDocument(**network) for network in res.json()]

    async def get_network(self, network: str) -> NetworkInfo:
        res = await self.get(
            url = f"{self.uri}/v2/networks/{network}"
        )
        return NetworkInfo(**res.json())


    async def get_devices_in_network(self, network: str) -> List[DeviceInfo]:
        res = await self.get(
            url = f"{self.uri}/v2/networks/{network}/devices"
        )
        return [DeviceInfo(**device) for device in res.json()]

    async def apply_device_settings(self, mac_address: str, body: dict):
        res = await self.post(
            url = f"{self.uri}/v1/devices/{mac_address}/switch",
            body = body
        )
        response_data = res.json()

        # Check if any field in the response data has a value 'Failed'
        if any(value == 'Failed' for value in response_data.values()):
            raise ValueError(f"One or more settings failed to apply - {response_data}")

        return res

    async def get_device(self, mac_address: str) -> SwitchInfo:
        res = await self.get(
            url = f"{self.uri}/v1/devices/{mac_address}/switch",
        )
        response_data = res.json()

        return SwitchInfo(**response_data)
    
async def main():

    api = ReadyViewAPI(
        api_key = api_key,
        organization = org_id
    )

    network_documents = await api.get_networks()

    # Initialize an empty list to collect data for the DataFrame
    data = []


    def convert_seconds_to_dhms(total_seconds):
        # Create a timedelta object from the total seconds
        td = datetime.timedelta(seconds=total_seconds)

        # Extract days from timedelta
        days = td.days
        # Use divmod to get hours and minutes
        hours, remainder = divmod(td.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)

        return f"{days}:{hours}:{minutes}:{seconds}"

    async def fetch_device_info(device, network_document_id, network_info_name):
        # Initialize with default values or fetch device details as necessary
        switch = {}
        if device.status == "Online":
            try:
                switch_info = await api.get_device(device.mac_address)
                switch = switch_info.model_dump()
            except Exception as e: pass
                # # Handle the exception if needed or pass to collect partial information
                # print(f"Error fetching device info for {device.mac_address}")

        return {
            'Network ID': network_document.id,
            'Network Name': network_info.name,  # Assuming 'name' is an attribute of network_info
            'Device ID': device.id,  # Assuming 'id' is an attribute of the device object
            'Mac Address': device.mac_address,  # Assuming 'mac_address' is an attribute of the device object
            'Status': device.status,  # Assuming 'status' is an attribute of the device object
            'Uptime (Seconds)': switch.get('uptime', 0),  # Assuming 'uptime' is an attribute; default to 'Unknown' if not present
            'Uptime': convert_seconds_to_dhms(total_seconds=switch.get('uptime', 0))
            #
            # TODO expand columns you want to see in the report
            #---------------------------------------------------
        }

    # Loop through each network to gather information
    for network_document in network_documents:
        # Assuming 'get_network' returns detailed information about a single network
        network_info = await api.get_network(network=network_document.id)

        # Assuming 'get_devices_in_network' returns a list of devices within a network
        devices = await api.get_devices_in_network(network=network_document.id)

        # Prepare tasks for each device
        tasks = [fetch_device_info(device, network_document.id, network_info.name) for device in devices]

        # Gather device information concurrently
        devices_data = await asyncio.gather(*tasks)

        # Extend the main data list with the results
        data.extend(devices_data)

    # Once all data is collected, create a pandas DataFrame
    df = pd.DataFrame(
        data,
        columns=[
            'Network ID',
            'Network Name',
            'Device ID',
            'Mac Address',
            'Status',
            'Uptime (Seconds)',
            'Uptime',
            #
            # TODO expand columns you want to see in the report
            #---------------------------------------------------
        ]
    )

    # Show the DataFrame (optional, for verification purposes)
    print(df)
    
asyncio.run(main())