#
# Applies a template to devices in a network
# Authentication uses org_id and api_key, found under "Account" at https://ui.readylinks.io
#
# To execute: python3 readylinks_async_template_automation.py

import asyncio
import httpx
import json

org_id = "your-org-key"
api_key = "your-api-key"
auth_creds = (org_id, api_key)


async def get_templates():
    url = "https://api.readylinks.io/v1/devices/switch/templates"
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                url, auth=auth_creds, headers={"accept": "application/json"}
            )
            templates = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)

    return templates


async def apply_template(mac, template_id):
    url = "https://api.readylinks.io/v1/devices/" + mac + "/switch/template"
    body = {"template_id": template_id}
    try:
        async with httpx.AsyncClient() as client:
            response = await client.post(
                url,
                auth=auth_creds,
                json=body,
                timeout=None,
            )
            template_status = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)
    return template_status


async def get_networks():
    url = "https://api.readylinks.io/v1/networks"
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                url, auth=auth_creds, headers={"accept": "application/json"}
            )
            networks = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)

    return networks


async def get_devices_in_network(network_id):
    url = "https://api.readylinks.io/v1/networks/" + network_id + "/devices"
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                url, auth=auth_creds, headers={"accept": "application/json"}
            )
            devices = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)

    return devices


async def main():

    # AUTOMATION - Apply a template to all devices in a network
    # ------------------------------------------------------------

    # Get devices in network, all network IDs may be found in ReadyView or via API
    net_id = "Ua3bV8v3FSjRrVZrzA0V"
    net_devices = await get_devices_in_network(net_id)
    net_devices = json.loads(net_devices)

    # Async apply template to all devices in a network
    template_id = "7rDZQY14eRMZWA0v4TbA"
    tasks = []
    for device in net_devices:
        mac = device["mac"]
        tasks.append(apply_template(mac, template_id))
        # The API will handle instances where a device is already on the template, or if the template does not match the device type
    results = await asyncio.gather(*tasks)
    print(results)


asyncio.run(main())