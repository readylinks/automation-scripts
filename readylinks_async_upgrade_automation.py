#
# Updates devices in a network
# Authentication uses org_id and api_key, found under "Account" at https://ui.readylinks.io
#
# To execute: python3 readylinks_async_upgrade_automation.py

import httpx
import json
import asyncio

org_id = "your-org-key"
api_key = "your-api-key"
auth_creds = (org_id, api_key)


## Async function to update device
async def update_device(mac):
    url = "https://api.readylinks.io/v1/devices/" + mac + "/switch/upgrade"
    try:
        async with httpx.AsyncClient() as client:
            response = await client.post(
                url,
                auth=auth_creds,
                timeout=None,
            )
            update_status = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(f"HTTP error occurred: {err}")
        print(f"HTTP status code: {err.response.status_code}")
        print(f"Response body: {err.response.text}")
        update_status = f"Error occurred while updating device {mac}: {err}"
    return update_status


# Async function to get networks in your organization
async def get_networks():
    url = "https://api.readylinks.io/v1/networks"
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                url, auth=auth_creds, headers={"accept": "application/json"}
            )
            networks = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)
    return networks


# Async function to get devices in your organization
async def get_devices_in_network(network_id):
    url = "https://api.readylinks.io/v1/networks/" + network_id + "/devices"
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                url, auth=auth_creds, headers={"accept": "application/json"}
            )
            devices = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)

    return devices


async def main():
    # ------------------------------------------------------------
    # ENTER TARGET NETWORK ID HERE , all network IDs may be found in ReadyView or via API

    # Example Network ID
    net_id = "50UtVuJPaVGf4p76c6rg"

    # ------------------------------------------------------------

    net_devices = await get_devices_in_network(net_id)
    net_devices = json.loads(net_devices)

    # Update all devices in network
    print(f"\n\n Updating devices in network {net_id}...")
    print(f"Please wait, this may take a few minutes...")
    for i in range(5):
        print(f".")

    # Create a list of tasks to update the devices concurrently
    tasks = []
    for device in net_devices:
        target_mac = device["mac_address"]
        print(f"Updating device {target_mac}...")
        task = asyncio.create_task(update_device(target_mac))
        tasks.append(task)

    # Wait for all tasks to complete
    responses = await asyncio.gather(*tasks)
    for response in responses:
        print(response)


# Run the main function
asyncio.run(main())