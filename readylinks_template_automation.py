#
# Applies a template to devices in a network
# Authentication uses org_id and api_key, found under "Account" at https://ui.readylinks.io
#
# To execute: python3 readylinks_template_automation.py

# Import packages
import httpx
import json

# Authentication
org_id = "your-org-key"
api_key = "your-api-key"
auth_creds = (org_id, api_key)

# Return available templates in your organization
def get_templates():
    url = "https://api.readylinks.io/v1/devices/switch/templates"
    try:
        response = httpx.get(
            url, auth=auth_creds, headers={"accept": "application/json"}
        )
        templates = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)

    return templates


# Apply template to device by mac address
def apply_template(mac, template_id):
    url = "https://api.readylinks.io/v1/devices/" + mac + "/switch/template"
    body = {"template_id": template_id}
    try:
        response = httpx.post(
            url,
            auth=auth_creds,
            json=body,
            timeout=None,
        )
        template_status = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)
    return template_status


# Get networks in your organization
def get_networks():
    url = "https://api.readylinks.io/v1/networks"
    try:
        response = httpx.get(
            url, auth=auth_creds, headers={"accept": "application/json"}
        )
        networks = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)

    return networks


# Get devices in your organization
def get_devices_in_network(network_id):
    url = "https://api.readylinks.io/v1/networks/" + network_id + "/devices"
    try:
        response = httpx.get(
            url, auth=auth_creds, headers={"accept": "application/json"}
        )
        devices = json.dumps(response.json(), indent=4)
    except httpx.HTTPError as err:
        print(err)
        raise SystemExit(err)

    return devices


def main():
    # Optional - Apply a single device template
    # ------------------------------------------------------------
    # target_mac = "0013ba0aTARGET"
    # template_id = "ZZvJ3vTd6If8Mq6lFxsS"
    # print(f"Applying template {template_id} to device {target_mac}...")
    # response = apply_template(target_mac, template_id)
    # print(response)

    # Apply a template to all devices in a network
    # ------------------------------------------------------------
    # Get devices in network, all network IDs may be found in ReadyView or via API
    net_id = "Ua3bV8v3FSjRrVZrzA0V"
    net_devices = get_devices_in_network(net_id)
    net_devices = json.loads(net_devices)
    # Apply template to all devices in a network, all template IDs may be found in ReadyView or via API
    template_id = "7rDZQY14eRMZWA0v4TbA"
    print(f"\n\n Applying template {template_id} to devices in network {net_id}...")
    print(f"Please wait, this may take a few minutes...")
    print(
        f"\n\n The API will handle cases where a device does not match the model of the template"
    )

    for device in net_devices:
        target_mac = device["mac_address"]
        print(f"Applying template {template_id} to device {target_mac}...")
        response = apply_template(target_mac, template_id)
        print(response)

main()
